package car

import (
	"bitbucket.org/andresPirona/golangcrud/db"
	"bitbucket.org/andresPirona/golangcrud/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetCars(c *gin.Context) {
	var cars []models.Car
	db := db.GetDB()
	db.Find(&cars)
	c.JSON(200, cars)
}

func CreateCar(c *gin.Context) {
	var car models.Car
	var db = db.GetDB()
	if err := c.BindJSON(&car); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	db.Create(&car)
	c.JSON(http.StatusOK, &car)
}

func UpdateCar(c *gin.Context) {
	id := c.Param("id")
	var car models.Car

	db := db.GetDB()
	if err := db.Where("id = ?", id).First(&car).Error; err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	c.BindJSON(&car)
	db.Save(&car)
	c.JSON(http.StatusOK, &car)
}

func DeleteCar(c *gin.Context) {
	id := c.Param("id")
	var car models.Car
	db := db.GetDB()

	if err := db.Where("id = ?", id).First(&car).Error; err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	db.Delete(&car)
}
