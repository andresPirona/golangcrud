package car_test

import (
	"bitbucket.org/andresPirona/golangcrud/db"
	"bitbucket.org/andresPirona/golangcrud/routes"
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var r *gin.Engine

func TestInitAll(t *testing.T) {
	gin.SetMode(gin.TestMode)
	db.Init()
	r = routes.SetupRoutes()
}

func TestTaskHandler(t *testing.T) {

	assertV := assert.New(t)

	urlBase := "/api/v1"

	factor := time.Now().String()

	tests := []struct {
		messageError       string
		url                string
		expectedStatusCode int
		method             string
		body               []byte
	}{
		{
			messageError:       "Car Created - Error",
			url:                urlBase + "/cars/",
			expectedStatusCode: 200,
			method:             "POST",
			body:               []byte(`{"description":"Test Car. ` + factor + `", "price": 20000}`),
		}, {
			messageError:       "Car Listed - Error",
			url:                urlBase + "/cars/",
			expectedStatusCode: 200,
			method:             "GET",
			body:               nil,
		}, /*{
			message:            "Tasks Deleted - Success",
			url:                urlBase + "/tasks/e8224711-7225-401b-90ef-007f6f026a75",
			expectedStatusCode: 200,
			method:             "DELETE",
			body:               nil,
		},*/{
			messageError:       "Car Updated - Error",
			url:                urlBase + "/cars/1",
			expectedStatusCode: 200,
			method:             "PUT",
			body:               []byte(`{"description":"Car Updated.", "price": 10}`),
		},
		// TODO not all cases are covered
	}

	for _, uniTest := range tests {

		req, err := http.NewRequest(uniTest.method, uniTest.url, bytes.NewBuffer(uniTest.body))
		assertV.NoError(err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)

		assertV.Equal(uniTest.expectedStatusCode, w.Code, uniTest.messageError)

	}

}
