package main

import (
  "bitbucket.org/andresPirona/golangcrud/db"
  "bitbucket.org/andresPirona/golangcrud/routes"
  "log"
  "os"
)

func main() {
  DoAll()
}

func DoAll()  {

  log.Println("Starting server..")
  db.Init()
  r := routes.SetupRoutes()

  port := os.Getenv("PORT")
  if port == "" {
    port = "5000"
  }

  r.Run(":" + port)

}
