package routes

import (
	CarController "bitbucket.org/andresPirona/golangcrud/controllers"
	"github.com/gin-gonic/gin"
)

func SetupRoutes() *gin.Engine  {
	r := gin.New()
	gin.SetMode(gin.ReleaseMode)

	v1 := r.Group("/api/v1")
	{
		tasks := v1.Group("/cars")
		{
			tasks.GET("/", CarController.GetCars)
			tasks.POST("/", CarController.CreateCar)
			tasks.PUT("/:id", CarController.UpdateCar)
			tasks.DELETE("/:id", CarController.DeleteCar)
		}
	}

	return r
}
