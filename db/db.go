package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/lib/pq"
	"log"
	"os"
)

var db *gorm.DB
var err error

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// Init creates a connection to mysql database and 
// migrates any new models
func Init() {
	user := getEnv("MYSQL_USER", "bdcfb95d0423e5")
	password := getEnv("MYSQL_PASSWORD", "0fa5d6d8")
	host := getEnv("MYSQL_HOST", "us-cdbr-iron-east-05.cleardb.net")
	port := getEnv("MYSQL_PORT", "3306")
	database := getEnv("MYSQL_DATABASE", "heroku_ceb7d9dd88218e1")

  dbinfo := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
  	user,
  	password,
  	host,
  	port,
  	database)

  db, err = gorm.Open("mysql", dbinfo)
  if err != nil {
    log.Println("Failed to connect to database")
    panic(err)
  }
  log.Println("Database connected")

}

//GetDB ...
func GetDB() *gorm.DB {
  return db
}

func CloseDB() {
	db.Close()
}