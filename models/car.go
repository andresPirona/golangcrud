package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

// Car "Object
type Car struct {
	ID          int64      `json:"id" db:"id"`
	Description string     `json:"description" db:"description" binding:"required"`
	Price       float64    `json:"price" db:"price" binding:"required"`
}

func (task *Car) BeforeUpdate(scope *gorm.Scope) error {
	scope.SetColumn("UpdatedAt", time.Now())
	return nil
}
