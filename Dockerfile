# Start from golang
FROM golang:latest

# Add Maintainer Info
LABEL maintainer="Andres Pirona <andrespirona92@gmail.com>"

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/bitbucket.org/andresPirona/golangcrud/

# Copy everything from the current directory to the PWD(Present Working Directory) inside the container
COPY . .

# Set the Current Working Directory inside the container
# WORKDIR $GOPATH/src/bitbucket.org/andresPirona/golangcrud/

# Install all dependencies
RUN go get -d -v ./...

# Install the package
RUN go install -v ./...

RUN go build -o golangcrud

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim"]

EXPOSE 8080

CMD ["./golangcrud"]